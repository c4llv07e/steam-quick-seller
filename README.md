# steam quick seller script

Idea from (Russian): https://www.youtube.com/watch?v=YkJAmLtlTyA

# How to use

```
python main.py
```

Then enter the AppID of the game and items with an empty string at the end.

## Example:

```
Enter appID (440): 440
Enter the list of items ending with an empty line:
Scream Fortress XII War Paint Case
Mann Co. Supply Crate Key

Url: https://steamcommunity.com/market/multisell?&appid=440&contextid=2&items[]=Scream%20Fortress%20XII%20War%20Paint%20Case&items[]=Mann%20Co.%20Supply%20Crate%20Key&qty[]=0
```
